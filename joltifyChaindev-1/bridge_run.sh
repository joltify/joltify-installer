#!/usr/bin/expect
set timeout 60

spawn %go_bin%/bridge_service -home %jolt_home%/config -key operator.key -grpc-port-atom %atom_rpc% -http-port-atom %atom_http% -pub-ws-ETHendpoint %eth_ws% -pub-ws-endpoint %bsc_ws% -coschain-rollback-gap 5 -pubchain-rollback-gap 5  -peer /ip4/%bridge_ip%/tcp/6668/p2p/%bridge_peer%

expect "please input the password:" {
  send "%passcode%\n\n"
}
trap {
    send \x03
    send_user "You pressed Ctrl+C\n"
} SIGINT

interact
