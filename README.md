# JoltifyChain Installer

This Installer is forked from osmosis installer.

Simple installer with the following features:
- Install dependencies
- Create swap file (if needed)
- Join testnet or mainnet
- Install the joltify software
- Launch the joltify chain automatically
- Check the sync progress
- Become a validator

## Installing Environment
The installer requires a Linux/arm64 or Linux/amd64 opearting system with with python installed.
```shell
# We assume you have a clean environment with python installed.

# 1. Going to the $HOME
cd

# 2. Cloning the 'joltify-installer'
git clone https://gitlab.com/joltify/joltify-installer.git

# 3. Checkout to expected version or branch
git checkout v0.1.0

# 4. Going to the 'joltify-installer' folder
cd joltify-installer

# 5. Run 'pip3 install -r requirements.txt' to install dependencies
pip3 install -r requirements.txt

# 6. prepare config file carefully
testnet: cp .env.example .env.testnet
mainnet: cp .env.example .env.mainnet

# 7. Run 'joltify-installer.py' file without default setting
python3 joltify-installer.py
```

## Installing Flow:
1. Prepare config file
   1. if you want to install mainnet node, run `cp .env.example .env.mainnet`, or for the testnet, run `cp .env.example .env.testnet`
   2. modify the config file options.
2. Basic packages Installation
(*currently only available for `linux` user*)
   1. Updating linux packages
   2. Installing make and GCC
   3. Installing Go
   4. Installing binary `joltify` (v0.1.0 for mainnet)
3. Select an option on the following menu
   ```
   Please choose an option you want to run:
   1) Run a Joltify Full Node
   2) Check Joltify Node sync status
   3) Become a Joltify Validator (require a synced full node)
   4) Run a Joltify Bridge Service (currently not available)
   5) Quit
   ```

## Validator Operations
### Becoming a Validator
1. Loading paramters from the .env file
2. (Optional) Editing parameters for becoming a validator
   - Validator Moniker
   - Security Contact
   - Details
   - Identity
   - Website
   - Staking Amount
   - Validator Name
   - Commission Rate
   - Max Commission Rate
   - Max Commission Change Rate
   - Min Self Delegation
2. Checking balance
3. Sending the `creat-validator` transaction


If you do not join as validator via `jolitfy-installer.py` program, you can still become a validator by manually sending the following transaction.

```shell
joltify tx staking create-validator \
   --home=<joltify_home_folder> \
   --from=<key_name> \
   --amount=<staking_amount_ujolt> \
   --pubkey=$(joltify tendermint show-validator) \
   --moniker=<moniker_id_of_your_node> \
   --security-contact=<security contact email/contact method> \
   --chain-id=<chain-id> \
   --commission-rate=<commission_rate> \
   --commission-max-rate=<maximum_commission_rate> \
   --commission-max-change-rate=<maximum_rate_of_change_of_commission> \
   --min-self-delegation=<min_self_delegation_amount> \
   --keyring-backend='file' \
   -y
```

You can use the following command to get your `operator name` and `public key`.
```shell
joltify keys list --keyring-backend file --home YourJoltifyHomeFolder
```

### Topping up `jolt` for your validator node
After becoming a validator, you can also further top up more `jolt` to your validator account.
```shell
joltify tx staking delegate \
    <validator_addr> \
    <staking_amount>jolt \
    --from=<key_name> \
    --home=<joltify_home_folder> \
    --chain-id=<chain-id> \
    --keyring-backend='file' \
    -y 
```
Note: `validator_addr` starts with 'joltvaloper'.

use this command to get validator_addr: `joltify keys show <key-name> --bech=val`

### Unbonding the staked coins
You can withdraw your staked coins by using the following command:
```shell
joltify tx staking unbond \
    <validator_addr> \
    <staking_amount>jolt \
    --from=<key_name> \
    --home=<joltify_home_folder> \
    --chain-id=<chain-id> \
    --keyring-backend='file' \
    -y
```

## Configuration
You can add these items in the configuration file to override the default settings. Before making any modifications, please make sure you understand the purpose of each item.

**LocalChain**
```toml
installHome
version
chainID
```

**PeerChain**
```toml
# rpc: the rpc address of the peer chain
rpc
# api: the rest address of the peer chain
api
# peers: the p2p address of the peer chain
# mainnet peers: "1063f14d45345b3ed0c0e533bda52e78de754d24@37.27.18.236:26656,34e0148645d860f45db510981f15da94b6b9d6ce@168.119.171.59:26656,2dd612a8c7268da6685c612f956eba2f42ae06f3@64.176.42.11:26656"
peers
```

**Ports**
```toml
# parameters in `app.toml` file
apiServer
grpcServer
grpcWebServer

# parameters in `config.toml` file
abciApp
rpcLAddr
p2pLAddr
pprofLAddr
```

**Pruning**
```toml
# default: the last 362880 states are kept, pruning at 10 block intervals
# nothing: all historic states will be saved, nothing will be deleted (i.e. archiving node)
# everything: 2 latest states will be kept; pruning at 10 block intervals.
# custom: allow pruning options to be manually specified through 'pruning-keep-recent', and 'pruning-interval'
pruning
pruningKeepRecent
pruningInterval
```

**Node**
```toml
# full: Full Node (download chain data and run locally)
# client: Client Node (setup a daemon and query a public RPC)
nodeType
# desired node name, cant be blank
nodeName
# Use swap if less than 32GB RAM are detected
# true or false
swapOn
# There have been reports of replay from genesis needing extra swap (up to 64GB) to prevent OOM errors.
# Use swap if less than 64GB RAM are detected
# true or false
extraSwapOn
# Data Sync Selection
# genesis: Start at block 1 and automatically upgrade at upgrade heights (replay from genesis)
# exit: Only install the daemon
dataSync
# Replaying Setup
# true: start cosmovisor as a background service and replay now
# false: do not start replay now
replayNow
```

**Validator**
```toml
# The name of your validator and this can not be blank
validatorMoniker
# The validator's (optional) security contact email
securityContact
# The validator's (optional) details
details
# The optional identity signature (ex. UPort or Keybase)
identity
# The validator's (optional) website
website
# the staking amount currently being self delegated to the validator
stakeAmount
# the commission rate charged to delegators
commissionRate
# the maximum rate the commission can be set to
commissionMaxRate
# the maximum rate the commission can be changed in one change
commissionMaxChangeRate
# the minimum self delegation required to be a validator
minSelfDelegation
```