# Use the official Ubuntu image as the base image
FROM ubuntu:latest

# Update package list and install necessary packages
RUN apt-get update && \
    apt-get install -y vim python3 python3-pip wget jq bc

# Add a new user named 'tester'
RUN useradd -ms /bin/bash tester

# Set the default user to 'tester'
USER tester

# Set the working directory to the home directory of the 'tester' user
WORKDIR /home/tester

# Set the default command to run when the container starts
CMD ["bash"]
