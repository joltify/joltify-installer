import pwd
import subprocess
import os
import platform
import time
import datetime
import shutil
import getpass
import json
from enum import Enum
from dotenv import dotenv_values
from distutils.util import strtobool


repo = "https://gitlab.com/joltify/joltify_lending"
passcode = None
decimal = 6
denom = "ujolt"
bridgeName = "bridge"
defaultPorts = {
    # app.toml
    "apiServer": "tcp://localhost:1317",
    "grpcServer": "localhost:9090",
    "grpcWebServer": "localhost:9091",

    # config.toml
    "abciApp": "tcp://127.0.0.1:26658",
    "rpcLAddr": "tcp://127.0.0.1:26657",
    "p2pLAddr": "tcp://0.0.0.0:26656",
    "pprofLAddr": "localhost:6060",
}
bridgeTestnetCfg = {
    "atom_rpc": "158.179.16.185:9090",
    "atom_http": "http://158.179.16.185:26657",
    "eth_ws": "ws://65.109.48.184:8545",
    "bsc_ws": "ws://65.109.48.184:8456",
    "bridge_ip": "65.109.48.184",
    "bridge_http": "http://65.109.48.184:8321",

    "bridgeVersion": "v1.1.0",
}


class NetworkType(str, Enum):
    MAINNET = "mainnet"
    TESTNET = "testnet"


class NodeType(str, Enum):
    FULL = "full"
    CLIENT = "client"


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def isfloat(numStr):
    try:
        float(numStr)
        return True
    except ValueError:
        return False


def colorprint(prompt: str):
    print(bcolors.OKGREEN + prompt + bcolors.ENDC)


# append content to file if it doesn't exist
def append(content, file):
    subprocess.run(["grep -qxF '{c}' {f} || echo '{c}' >> {f}".format(c=content, f=file)], shell=True, env=my_env)

def runCMD(cmd, success_prompt, fail_prompt, show_log=True):
    if show_log:
        subprocess.run([cmd], shell=True, env=my_env)
    else:
        subprocess.run([cmd], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)
    check = subprocess.Popen(["echo $?"], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = check.stdout.read().decode("utf-8").strip()
    if output != "0":
        print(bcolors.FAIL + fail_prompt + bcolors.ENDC)
        quit()
    else:
        if success_prompt != "":
            print(bcolors.OKGREEN + success_prompt + bcolors.ENDC)
    return True

def supervisorctlRemove(name):
    subprocess.run(["{sudo}supervisorctl status {n} | grep RUNNING && {sudo}supervisorctl stop {n}".format(n=name, sudo=sudoprefix)], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)
    subprocess.run(["{sudo}supervisorctl status {n} | grep {n} | grep -v '(no such process)' && {sudo}supervisorctl remove {n}".format(n=name, sudo=sudoprefix)], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)


def supervisorctlStart(name):
    supervisorctlRemove(name)
    subprocess.run([sudoprefix + "supervisorctl reread"], shell=True, env=my_env)
    subprocess.run([sudoprefix + "supervisorctl add {n}".format(n=name)], shell=True, env=my_env)
    subprocess.run([sudoprefix + "supervisorctl update"], shell=True, env=my_env)


def completeCosmovisor():
    print(bcolors.OKGREEN +
          "Congratulations! You have successfully completed setting up an Joltify full node!")
    print(bcolors.OKGREEN +
          "The cosmovisor service is currently running in the background")
    print(bcolors.OKGREEN + "To see the status of cosmovisor, run the following command: 'sudo supervisorctl status cosmovisor'")
    colorprint(
        "To see the live logs from cosmovisor, run the following command: 'sudo supervisorctl log -f cosmovisor'")
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def completeJoltify():
    print(bcolors.OKGREEN +
          "Congratulations! You have successfully completed setting up an Joltify full node!")
    print(bcolors.OKGREEN +
          "The joltify service is currently running in the background")
    print(bcolors.OKGREEN + "To see the status of the joltify daemon, run the following command: 'sudo supervisorctl status joltify'")
    colorprint(
        "To see the live logs from the joltify daemon, run the following command: 'sudo supervisorctl log -f joltify'")
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def complete():
    print(bcolors.OKGREEN +
          "Congratulations! You have successfully completed setting up a Joltify full node!")
    print(bcolors.OKGREEN + "The joltify service is NOT running in the background")
    print(bcolors.OKGREEN +
          "You can start joltify with the following command: 'joltify start'" + bcolors.ENDC)
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def partComplete():
    print(bcolors.OKGREEN +
          "Congratulations! You have successfully completed setting up the Joltify daemon!")
    print(bcolors.OKGREEN +
          "The joltify service is NOT running in the background, and your data directory is empty")
    print(bcolors.OKGREEN + "If you intend to use joltify without syncing, you must include the '--node' flag after cli commands with the address of a public RPC node" + bcolors.ENDC)
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def clientComplete():
    print(bcolors.OKGREEN +
          "Congratulations! You have successfully completed setting up an Joltify client node!")
    colorprint(
        "DO NOT start the joltify daemon. You can query directly from the command line without starting the daemon!")

def bridgeComplete():
    print(bcolors.OKGREEN +
          "Congratulations! You have successfully completed setting up an Joltify Bridge service!")
    print(bcolors.OKGREEN +
          "The joltify bridge service is currently running in the background")
    print(bcolors.OKGREEN + "To see the status of the joltify bridge daemon, run the following command: 'sudo supervisorctl status bridge'")
    colorprint(
        "To see the live logs from the joltify bridge daemon, run the following command: 'sudo supervisorctl log -f bridge'")
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def replayComplete():
    subprocess.run(["clear"], shell=True)
    print(bcolors.OKGREEN +
          "Congratulations! You are currently replaying from genesis in a background service!")
    print(bcolors.OKGREEN + "To see the status of cosmovisor, run the following command: 'sudo supervisorctl status cosmovisor'")
    colorprint(
        "To see the live logs from cosmovisor, run the following command: 'sudo supervisorctl log -f cosmovisor'")
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def replayDelay():
    joltifyName = config.get('joltifyName', 'joltify')
    print(bcolors.OKGREEN +
          "Congratulations! Joltify is ready to replay from genesis on your command!")
    print(bcolors.OKGREEN +
          "YOU MUST MANUALLY INCREASE ULIMIT FILE SIZE BEFORE STARTING WITH `ulimit -n 200000`")
    print(bcolors.OKGREEN +
          "Use the command `cosmovisor start` to start the replay from genesis process")
    print(bcolors.OKGREEN +
          "It is recommended to run this in a tmux session if not running as a background service")
    print(bcolors.OKGREEN + "You must use `cosmovisor start` and not `{} start` in order to upgrade automatically".format(joltifyName) + bcolors.ENDC)
    waitForContinue("Press any key to return to the main menu...")
    mainMenu()


def getHeight(url):
    cmd = "curl {}/block?height?".format(url)
    try:
        querylock = subprocess.Popen([cmd], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
        resp = json.loads(querylock.stdout.read().decode("utf-8"))
    except:
        return None
    return resp.get("result", {}).get("block", {}).get("header", {}).get("height", None)

def isSynced():
    synced = False
    while True:
        localHeight = getHeight("http://{}".format(getServiceAddr(config.get('rpcLAddr', ''))))
        pubHeight = getHeight(config.get('rpc', ''))
        if localHeight is None or pubHeight is None:
            print(bcolors.WARNING + "Failed to get the current block height of the local chain or the public peer chain." + bcolors.ENDC)
            time.sleep(5)
        else:
            progress = int(localHeight)/int(pubHeight)
            break
    if progress >= 1:
        synced = True
    return synced
        

def checkSyncProgress():
    subprocess.run(["clear"], shell=True)
    progress = 0
    localNoneResponse = 0
    pubNoneResponse = 0
    while progress != 1:
        # TODO: setup url parameter
        colorprint("Query the current block height of your local chain...")
        localHeight = getHeight("http://{}".format(getServiceAddr(config.get('rpcLAddr', ''))))
        if localHeight is None:
            time.sleep(3)
            localNoneResponse += 1
            if localNoneResponse > 10:
                subprocess.run(["clear"], shell=True)
                print(bcolors.WARNING + "The local chain might encounter some issues to sync.\n" + bcolors.ENDC)
                waitForContinue("Press any key to return to the main menu...")
                mainMenu()
            continue
        colorprint("Local Chain Height: {}\n".format(localHeight))
        colorprint("Query the current block height of your public peer chain...")
        pubHeight = getHeight(config.get('rpc', ''))
        if pubHeight is None:
            time.sleep(3)
            pubNoneResponse += 1
            if pubNoneResponse > 10:
                subprocess.run(["clear"], shell=True)
                print(bcolors.WARNING + "The public peer chain might encounter some issues for querying the data.\n" + bcolors.ENDC)
                waitForContinue("Press any key to return to the main menu...")
                mainMenu()
            continue
        colorprint("Public Peer Chain Height: {}\n".format(pubHeight))
        if int(pubHeight) - int(localHeight) <= 1:
            break
        progress = int(localHeight)/int(pubHeight)
        if localNoneResponse > 0 or pubNoneResponse > 0:
            localNoneResponse = 0
            pubNoneResponse = 0
            subprocess.run(["clear"], shell=True)
            print(bcolors.OKGREEN + "Sync Progress: {}%\n".format(round(progress * 100, 2)) + bcolors.ENDC, end="\r")
        else:
            print(bcolors.OKGREEN + "Sync Progress: {}%\n".format(round(progress*100, 2)) + bcolors.ENDC, end="\r")
        waitOrReturn = input(bcolors.OKGREEN + """
Input (q)uit to return to the main menu or press any key to refresh: """ + bcolors.ENDC)
        if waitOrReturn.lower() == "q":
            subprocess.run(["clear"], shell=True)
            mainMenu()
        subprocess.run(["clear"], shell=True)
    waitForContinue("Your local chain is up to date.\nPress any key to return to the main menu...")
    subprocess.run(["clear"], shell=True)
    mainMenu()

def getKeyInfo():
    getPasscode()
    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    cmd = "{} keys list --keyring-backend file --home {}".format(joltifyName, jolt_home) + "| sed 's/-/ /' | grep 'name\|address' | awk '{print $2}'"
    extractInfo = subprocess.Popen([cmd], shell=True,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)

    extractInfo.stdin.write(f"{passcode}\n".encode("utf-8"))
    extractInfo.stdin.flush()

    b = extractInfo.stderr.read().decode("utf-8").strip()
    print(bcolors.FAIL + b + bcolors.ENDC)

    address, operatorName = extractInfo.stdout.read().decode("utf-8").strip().split("\n")

    cmd2 = "{} tendermint show-validator --home {}".format(joltifyName, jolt_home)
    extractPubkeyEd25519 = subprocess.Popen([cmd2],  shell=True,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
    pubkeyEd25519 = extractPubkeyEd25519.stdout.read().decode(("utf-8")).strip()
    subprocess.run(["clear"], shell=True)
    return operatorName, address, pubkeyEd25519


def getBalance(addr):
    colorprint("Checking your balance...")
    cmd = "curl {}/cosmos/bank/v1beta1/balances/{}/by_denom?denom={}".format(config.get('api'), addr, denom)
    getBalance = subprocess.Popen([cmd], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE, env=my_env)
    jsonString = getBalance.stdout.read().decode("utf-8")
    try:
        balanceU = json.loads(jsonString)["balance"]["amount"]
    except:
        print(bcolors.WARNING + "Failed to get balance with Error: \n" +bcolors.ENDC + getBalance.stderr.read().decode("utf-8"))
        return 0
    balance = float(balanceU) / (10 ** decimal)
    return balance

def validInput(name, prompt, gt0: bool=False):
    while True:
        val = input(bcolors.OKGREEN + prompt + bcolors.ENDC)
        if isfloat(val):
            if gt0 and not (float(val) > 0):
                print(bcolors.WARNING + "{} must be greater than zero, please try again..." + bcolors.ENDC)
                continue
            break
        else:
            print(bcolors.WARNING + "{} is not valid, please try again.".format(name) + bcolors.ENDC)
    return val


def setValidatorParams():
    moniker = config.get('validatorMoniker', 'operator')
    securityContact = config.get('securityContact', '')
    details = config.get('details', '')
    identity = config.get('identity', '')
    website = config.get('website', '')
    stakeAmount = config.get('stakeAmount', '1000')
    commissionRate = config.get('commissionRate', '0.05')
    commissionMaxRate = config.get('commissionMaxRate', '0.2')
    commissionMaxChangeRate = config.get('commissionMaxChangeRate', '0.01')
    minSelfDelegation = config.get('minSelfDelegation', '1000')
    toQuit = False
    processing = True

    while processing:
        subprocess.run(["clear"], shell=True)
        colorprint("Current config Info:\n"
                    + "  Validator Moniker: {}\n".format(moniker)
                    + "  Security Contact: {}\n".format(securityContact)
                    + "  Details: {}\n".format(details)
                    + "  Identity: {}\n".format(identity)
                    + "  Website: {}\n".format(website)
                    + "  Staking Amount: {}\n".format(stakeAmount+denom[1:])
                    + "  Commission Rate: {}\n".format(commissionRate)
                    + "  Commission Max Rate: {}\n".format(commissionMaxRate)
                    + "  Commission Max Change Rate: {}\n".format(commissionMaxChangeRate)
                    + "  Min Self Delegation: {}\n\n".format(minSelfDelegation+denom[1:]))

        colorprint("""Do you want to continue with current parameters?
1) Yes, I want to continue
2) No, I want to reset the validator parameters
3) I don't want to become a validator anymore
                    """)
        option = input(bcolors.OKGREEN + 'Enter Choice: ' + bcolors.ENDC)
        if option == "1":
            subprocess.run(["clear"], shell=True)
            processing = False
        elif option == "2":
            subprocess.run(["clear"], shell=True)
            colorprint("The validator's name.")
            moniker = input(bcolors.OKGREEN + ">>> Validator Moniker:" + bcolors.ENDC)
            colorprint("The validator's (optional) security contact email.")
            securityContact = input(bcolors.OKGREEN + ">>> Security Contact:" + bcolors.ENDC)
            colorprint("The validator's (optional) details.")
            details = input(bcolors.OKGREEN + ">>> Details:" + bcolors.ENDC)
            colorprint("The optional identity signature (ex. UPort or Keybase).")
            identity = input(bcolors.OKGREEN + ">>> Identity:" + bcolors.ENDC)
            colorprint("The validator's (optional) website.")
            website = input(bcolors.OKGREEN + ">>> Website:" + bcolors.ENDC)
            stakeAmount = validInput("Staking Amount", ">>> Staking Amount (e.g. 1000, which indicates '1000{}'): ".format(denom[1:]), True)
            commissionRate = validInput("Commission Rate", "The initial commission rate percentage.\n>>> Commission Rate (e.g. 0.05): ", False)
            commissionMaxRate = validInput("Commission Max Rate", "The maximum commission rate percentage.\n>>> Commission Max Rate (e.g. 0.2): ", False)
            commissionMaxChangeRate = validInput("Commission Max Change Rate", "The maximum commission change rate percentage (per day).\n>>> Commission Max Change Rate (e.g. 0.01): ", False)
            minSelfDelegation = validInput("Min Self Delegation", "The minimum self delegation required on the validator.\n>>> Min Self Delegation (e.g. 1000, which indicates '1000jolt'): ", False)
        elif option == "3":
            subprocess.run(["clear"], shell=True)
            processing = False
            toQuit = True

    return moniker, securityContact, details, identity, website, stakeAmount, commissionRate, commissionMaxRate, commissionMaxChangeRate, minSelfDelegation, toQuit


def joinValidator():
    # 0. set up validator parameters
    moniker, securityContact, details, identity, website, stakeAmount, commissionRate, commissionMaxRate, commissionMaxChangeRate, minSelfDelegation, toQuit = setValidatorParams()
    if toQuit:
        subprocess.run(["clear"], shell=True)
        return

    # 1. extract pub key
    operatorName, address, pubkeyEd25519 = getKeyInfo()
    # 2. query balance
    balance = getBalance(address)
    # 3. (while loop) compare balance with required staking amount
    while balance < float(stakeAmount):
        colorprint("""You don't have enough amount of {d} to stake
 Your account {a} balance: {b} {d}\n  Required amount: {s} {d}

Please make sure you have at least '{s} {d}' on your account.
1) Yes, I already top up required amount of tokens for staking
2) No, I don't want to become a validator anymore.
        """.format(a=address, b=balance, s=stakeAmount, d=denom[1:]))

        option = input(bcolors.OKGREEN + "Enter choice: " + bcolors.ENDC)
        if option == "2":
            subprocess.run(["clear"], shell=True)
            return

        subprocess.run(["clear"], shell=True)
        time.sleep(3)
        balance = getBalance(address)
    while not isSynced():
        colorprint("Waiting for your local chain to sync with the public peer chain...")
        time.sleep(3)
    
    # 4. (if pass the balance check) join validators
    print(bcolors.OKGREEN + "Joining Validators..." + bcolors.ENDC)
    cmd = config.get('joltifyName', 'joltify') + " tx staking create-validator \
        --home={} \
        --amount={}{} \
        --pubkey='{}' \
        --moniker='{}' \
        --security-contact='{}'\
        --details='{}' \
        --identity='{}' \
        --website='{}' \
        --chain-id={} \
        --commission-rate='{}' \
        --commission-max-rate='{}' \
        --commission-max-change-rate='{}' \
        --min-self-delegation='{}' \
        --from={} \
        --keyring-backend='file' -o json -y".format(
            config.get('installHome'),
            stakeAmount, denom[1:],
            pubkeyEd25519,
            moniker,
            securityContact,
            details,
            identity,
            website,
            config.get('chainID'),
            commissionRate,
            commissionMaxRate,
            commissionMaxChangeRate,
            int(float(minSelfDelegation)*10**decimal),
            operatorName)

    joinValidatorList = subprocess.Popen([cmd], shell=True,
                                            stdin=subprocess.PIPE,
                                            stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
    joinValidatorList.stdin.write(f"{passcode}\n".encode("utf-8"))
    joinValidatorList.stdin.flush()

    err = joinValidatorList.communicate()[1].decode("utf-8")
    if err == "":
        result = json.loads(joinValidatorList.communicate()[0].decode("utf-8"))
        code = result.get("code", 0)
        if code == 0:
            colorprint("Successfully joined the validator list!")
            colorprint("Transaction Info: ")
            print(json.dumps(result, indent=2))
            waitForContinue("Press any key to continue...")
            return
        err = json.dumps(result, indent=2)

    print(bcolors.FAIL + "Error: " + bcolors.ENDC)
    print(err)

    colorprint("Please try to run the following command manually:")
    print(cmd)
    waitForContinue("Please press any key to exit.")


def getBridgePeer():
    try:
        peer = subprocess.Popen(["curl {}/p2pid".format(config.get('bridge_http'))], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return peer.stdout.read().decode("utf-8")
    except:
        print(bcolors.WARNING + "Error: Unable to get bridge peer id. Please check your bridge configuration." + bcolors.ENDC)
        return ""


def bridgeService():
    jolt_home = config.get('installHome')
    checkSyncProgress()

    colorprint("Setup Joltify Bridge Service Config...")
    getPasscode()

    os.chdir(os.path.expanduser(currentDir))
    subprocess.run(["cp {n}/bridge_run.sh {gp}/bin".format(n=config.get('chainID'), gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(["cp {n}/tokenlist.json {h}/config".format(n=config.get('chainID'), h=jolt_home)], shell=True, env=my_env)
    for key in bridgeTestnetCfg.keys():
        subprocess.run(["sed -i -E 's|%{key}%|{val}|g' {gp}/bin/bridge_run.sh".format(key=key, val=config.get(key), gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(["sed -i -E 's|%bridge_peer%|{val}|g' {gp}/bin/bridge_run.sh".format(val=getBridgePeer(), gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(["sed -i -E 's|%jolt_home%|{val}|g' {gp}/bin/bridge_run.sh".format(val=jolt_home, gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(["sed -i -E 's|%passcode%|{val}|g' {gp}/bin/bridge_run.sh".format(val=passcode, gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(["sed -i -E 's|%go_bin%|{gp}/bin|g' {gp}/bin/bridge_run.sh".format(gp=GOPATH)], shell=True, env=my_env)

    colorprint("Creating Joltify Bridge Service...")
    subprocess.run(["""echo '[program:{bn}]
environment=
    HOME={h}

command = {gp}/bin/bridge_run.sh
user = {u}

autostart = true
autorestart = false
startsecs = 3
stopsignal=SIGINT
stopasgroup=true
stopwaitsecs = 300

stdout_logfile = /var/log/{bn}.log
redirect_stderr = true
stdout_logfile_maxbytes = 200MB
stdout_logfile_backups = 5
' > {bn}.conf""".format(bn=bridgeName, h=HOME, u=USER, gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(
        ["sudo mv {}.conf /etc/supervisor/conf.d/".format(bridgeName)], shell=True, env=my_env)
    subprocess.run(["sudo supervisorctl reread"], shell=True, env=my_env)
    supervisorctlStart(bridgeName)
    bridgeComplete()


def cosmovisorService():
    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    colorprint("Creating Cosmovisor Service")

    append('# Setup Cosmovisor', HOME+'/.profile')
    append('export DAEMON_NAME={}'.format(joltifyName), HOME+'/.profile')
    append('export DAEMON_HOME="{}"'.format(jolt_home), HOME+'/.profile')
    append('export DAEMON_ALLOW_DOWNLOAD_BINARIES=true', HOME+'/.profile')
    append('export DAEMON_LOG_BUFFER_SIZE=512', HOME+'/.profile')
    append('export DAEMON_RESTART_AFTER_UPGRADE=true', HOME+'/.profile')
    append('export UNSAFE_SKIP_BACKUP=true', HOME+'/.profile')

    subprocess.run(["""echo '[program:cosmovisor]
environment=
    DAEMON_NAME=\"{jn}\",
    DAEMON_HOME=\"{jh}\",
    DAEMON_RESTART_AFTER_UPGRADE=true,
    DAEMON_ALLOW_DOWNLOAD_BINARIES=true,
    DAEMON_LOG_BUFFER_SIZE=512,
    UNSAFE_SKIP_BACKUP=true,
    HOME=\"{jh}\",

command = {gp}/bin/cosmovisor run start --home {jh}
user = {u}

autostart = true
autorestart = false
startsecs = 3
stopwaitsecs = 300

stdout_logfile = /var/log/cosmovisor.log
redirect_stderr = true
stdout_logfile_maxbytes = 200MB
stdout_logfile_backups = 5
' > cosmovisor.conf""".format(jn=joltifyName, jh=jolt_home, u=USER, gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(
        [sudoprefix + "mv cosmovisor.conf /etc/supervisor/conf.d/"], shell=True, env=my_env)
    subprocess.run([sudoprefix + "supervisorctl reread"], shell=True, env=my_env)
    supervisorctlStart("cosmovisor")


def joltifyService():
    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    colorprint("Creating Joltify Service...")
    subprocess.run(["""echo '[program:{jn}]
command = {gp}/bin/{jn} start --home {jh}
user = {u}

autostart = true
autorestart = false
startsecs = 3
stopwaitsecs = 300

stdout_logfile = /var/log/{jn}.log
redirect_stderr = true
stdout_logfile_maxbytes = 200MB
stdout_logfile_backups = 5
' > {jn}.conf""".format(jn=joltifyName, jh=jolt_home, u=USER, gp=GOPATH)], shell=True, env=my_env)
    subprocess.run(
        [sudoprefix + "mv {}.conf /etc/supervisor/conf.d/".format(joltifyName)], shell=True, env=my_env)
    subprocess.run([sudoprefix + "supervisorctl reread"], shell=True, env=my_env)
    supervisorctlStart(joltifyName)


def startReplayNow():
    replayNow = strtobool(config.get('replayNow', 'true'))
    if replayNow:
        cosmovisorService()
        waitForContinue("Press any key to continue...")
        replayComplete()
        return

    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    append('# Setup Cosmovisor', HOME+'/.profile')
    append('export DAEMON_NAME={}'.format(joltifyName), HOME+'/.profile')
    append('export DAEMON_HOME="{}"'.format(jolt_home), HOME+'/.profile')
    append('export DAEMON_ALLOW_DOWNLOAD_BINARIES=true', HOME+'/.profile')
    append('export DAEMON_LOG_BUFFER_SIZE=512', HOME+'/.profile')
    append('export DAEMON_RESTART_AFTER_UPGRADE=true', HOME+'/.profile')
    append('export UNSAFE_SKIP_BACKUP=true', HOME+'/.profile')
    subprocess.run(["clear"], shell=True)
    replayDelay()


def replayFromGenesisLevelDb():
    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    colorprint("Setting Up Cosmovisor...")
    os.chdir(os.path.expanduser(HOME))
    runCMD("go install cosmossdk.io/tools/cosmovisor/cmd/cosmovisor@latest", "Cosmovisor installed successfully", "Failed to install Cosmovisor")
    runCMD("mkdir -p "+jolt_home+"/cosmovisor", "", "Failed to create {}/cosmos folder".format(jolt_home))
    runCMD("mkdir -p "+jolt_home+"/cosmovisor/genesis", "", "Failed to create {}/cosmos/genesis folder".format(jolt_home))
    runCMD("mkdir -p "+jolt_home+"/cosmovisor/genesis/bin", "", "Failed to create {}/cosmos/genesis/bin folder".format(jolt_home))
    runCMD("mkdir -p "+jolt_home+"/cosmovisor/upgrades", "", "Failed to create {}/cosmos/upgrades folder".format(jolt_home))
    runCMD("cp {}/bin/{} {}/cosmovisor/genesis/bin".format(GOPATH, joltifyName, jolt_home), "", "Failed to copy {}/bin/{} to {}/cosmos/genesis/bin".format(GOPATH, joltifyName, jolt_home))
    startReplayNow()


def extraSwap():
    swapOn = strtobool(config.get('extraSwapOn', 'true'))
    setupSwap(64, swapOn)


def dataSyncSelection():
    dataSync = config.get('dataSync', 'genesis')
    if dataSync == "genesis":
        subprocess.run(["clear"], shell=True)
        extraSwap()
        replayFromGenesisLevelDb()
    else:
        subprocess.run(["clear"], shell=True)
        partComplete()


def pruningSelection():
    pruningSetting = config.get('pruning', 'default')
    jolt_home = config.get('installHome')
    runCMD("sed -i -E 's|pruning = \"default\"|pruning = \"{}\"|g' {}/config/app.toml".format(pruningSetting, jolt_home), "", "Failed to set pruning setting")
    
    if pruningSetting == "custom":
        runCMD("sed -i -E 's|pruning-keep-recent = \"0\"|pruning-keep-recent = \"{}\"|g' {}/config/app.toml".format(config.get('pruningKeepRecent', "0"), jolt_home), "", "Failed to set pruning-keep-every setting")
        runCMD("sed -i -E 's|pruning-interval = \"0\"|pruning-interval = \"{}\"|g' {}/config/app.toml".format(config.get('pruningInterval', "0"), jolt_home), "", "Failed to set pruning-interval setting")
    subprocess.run(["clear"], shell=True)


def customPortSelection():
    if not hasPortConfig(config):
        return

    jolt_home = config.get('installHome')
    for key, value in defaultPorts.items():
        # change app.toml values
        if key.endswith("Server"):
            subprocess.run(["sed -i -E 's|{}|{}|g' {}/config/app.toml".format(value, config.get(key), jolt_home)], shell=True)
        # change config.toml values
        else:
            subprocess.run(["sed -i -E 's|{}|{}|g' {}/config/config.toml".format(value, config.get(key), jolt_home)], shell=True)
    subprocess.run(["clear"], shell=True)


def getPasscode():
    global passcode
    if passcode:
        return

    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    prompt = bcolors.OKGREEN + 'Input your passcode to decrypt your validator key: ' + bcolors.ENDC
    while True:
        passcode = getpass.getpass(prompt=prompt, stream=None)
        cmd = "{} keys list --keyring-backend file --home {} --output json".format(joltifyName, jolt_home)
        list = subprocess.Popen([cmd], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)

        output = list.communicate(f"{passcode}\n".encode('utf-8'))
        err = output[1].decode('utf-8').strip()
        if not err:
            break
        prompt = bcolors.WARNING + 'Incorrect passcode, please try again: ' + bcolors.ENDC


def setupKeys():
    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    global passcode
    subprocess.run(["sed -i -E 's|0stake|0ujolt|g' "+jolt_home+"/config/app.toml"], shell=True)

    print(bcolors.OKGREEN + "Setup Joltify Validator Keys..." + bcolors.ENDC)
    repasscode = ""
    while True:
        passcode = getpass.getpass(
            prompt=bcolors.OKGREEN + 'Input desired passcode (at least 8 characters) to encrypt your validator key: ' + bcolors.ENDC,
            stream=None)
        if len(passcode) < 8:
            print(bcolors.FAIL + "The passcode is small than 8 characters" + bcolors.ENDC)
            continue
        repasscode = getpass.getpass(
            prompt=bcolors.OKGREEN + 'Re-Input desired passcode to encrypt your validator key: ' + bcolors.ENDC,
            stream=None)
        if passcode.strip() != repasscode.strip():
            print(bcolors.FAIL + "The two passwords do not match" + bcolors.ENDC)
            continue

        break

    p = subprocess.Popen(['{} keys add {} --keyring-backend file --home {}'.format(joltifyName, config.get("nodeName", "operator"), jolt_home)], 
                   shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
    passcode = passcode.strip()
    p.stdin.write(f"{passcode}\n".encode("utf-8"))
    p.stdin.flush()
    p.stdin.write(f"{passcode}\n".encode("utf-8"))
    p.stdin.flush()

    b = p.stderr.read().decode("utf-8").strip()
    print(bcolors.FAIL + b + bcolors.ENDC)

    input(bcolors.OKGREEN + "PLEASE CONFIRM YOU HAVE WRITE DOWN THE MNEMONIC(1) " + bcolors.ENDC)
    input(bcolors.OKGREEN + "PLEASE CONFIRM YOU HAVE WRITE DOWN THE MNEMONIC(2) " + bcolors.ENDC)
    input(bcolors.OKGREEN + "PLEASE CONFIRM YOU HAVE WRITE DOWN THE MNEMONIC(3) " + bcolors.ENDC)

    # we export the validator key
    p = subprocess.Popen(["{} keys export operator --keyring-backend file --home {} >> {}/config/operator.key".format(joltifyName, jolt_home, jolt_home)], shell=True,
                   stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
    p.stdin.write(f"{passcode.strip()}\n".encode("utf-8"))
    p.stdin.flush()
    p.stdin.write(f"{passcode.strip()}\n".encode("utf-8"))
    p.stdin.flush()


def setupFull():
    jolt_home = config.get('installHome')
    colorprint("Downloading and Replacing Genesis...")
    # subprocess.run(["curl {}/genesis |jq '.result''.genesis'>{}/config/genesis.json".format(config.get('rpc'), jolt_home)], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)
    runCMD("curl {}/genesis |jq '.result''.genesis'>{}/config/genesis.json".format(config.get('rpc'), jolt_home), "Genesis downloaded successfully", "Failed to download genesis")
    colorprint("Finding and Replacing Seeds...")
    subprocess.run(["sed -i -E 's|persistent_peers = \"\"|persistent_peers = \"" + config.get('peers', '') + "\"|g' "+jolt_home+"/config/config.toml"], shell=True)
    subprocess.run(["sed -i -E 's|max_subscriptions_per_client = 5|max_subscriptions_per_client = 5000|g' "+jolt_home+"/config/config.toml"], shell=True)
    subprocess.run(["sed -i -E 's|max_subscription_clients = 100|max_subscription_clients = 5000|g' "+jolt_home+"/config/config.toml"], shell=True)
    subprocess.run(["sed -i -E 's|0stake|0ujolt|g' "+jolt_home+"/config/app.toml"], shell=True)
    subprocess.run(["clear"], shell=True)
    customPortSelection()
    pruningSelection()
    dataSyncSelection()


def setupClient():
    jolt_home = config.get("installHome")
    subprocess.run(["sed -i -E 's|node = \"tcp://localhost:26657\"|node = \"{}\"|g' {}/config/client.toml".format(config.get('rpc'), jolt_home)], shell=True, env=my_env)

    subprocess.run(["clear"], shell=True)
    clientComplete()


def clientSettings():
    colorprint("Changing Client Settings...")
    jolt_home = config.get("installHome")
    subprocess.run(["sed -i -E 's|keyring-backend = \"os\"|keyring-backend = \"file\"|g' " + jolt_home +"/config/client.toml"], shell=True, env=my_env)
    subprocess.run(["sed -i -E 's|broadcast-mode = \"sync\"|broadcast-mode = \"sync\"|g' " + jolt_home+"/config/client.toml"], shell=True, env=my_env)


def backupKeys(path):
    if not os.path.exists(path):
        colorprint("No need for backup. The keyring-file folder does not exist.")
        return
    elif not os.listdir(path):
        colorprint("No need for backup. The keyring-file folder is empty.")
        return

    joltifyName = config.get('joltifyName', 'joltify')
    backupDir = "{}/{}_{}/{}_{}".format(HOME, joltifyName, "keys", datetime.datetime.now().strftime("%Y%m%d%H%M%S"), "keyring-file")
    os.makedirs(backupDir, exist_ok=True)
    print(bcolors.WARNING + "The keys will backup to " + backupDir + bcolors.ENDC)

    for file in os.scandir(path):
        dst = "{}/{}".format(backupDir, file.name)
        shutil.copyfile(file.path, dst)
        colorprint("Backing up {} to {}".format(file.path, dst))
        time.sleep(1)


def initNodeName():
    joltifyName = config.get('joltifyName', 'joltify')
    jolt_home = config.get('installHome')
    nodeName = config.get('nodeName')

    print(bcolors.FAIL + "AFTER NODE INITIALIZATION, ALL PREVIOUS JOLTIFY DATA WILL BE RESET" + bcolors.ENDC)
    if not inputYesNo("Are you sure to continue?"):
        if nodeType == NodeType.CLIENT.value:
            return
        else:
            mainMenu()
            return

    backupKeys(jolt_home+"/keyring-file")
    supervisorctlRemove("cosmovisor")
    subprocess.run(["rm -rf "+jolt_home], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)
    subprocess.run(["rm -rf "+HOME+"/."+joltifyName], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)

    client = "Full"
    if nodeType == NodeType.CLIENT.value:
        client = "Client"
    colorprint("Initializing Joltify {} Node '{}'".format(client, nodeName))

    subprocess.run(["source ~/.bashrc"],
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)

    subprocess.run(["chmod u+x {gp}/bin/{joltifyname}".format(gp=GOPATH,joltifyname=joltifyName)],
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)


    subprocess.run(["{} init {} --chain-id={} -o --home {}".format(joltifyName, nodeName, config.get('chainID', 'joltify_1729-1'), jolt_home)],
        stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True, env=my_env)

    clientSettings()
    if nodeType == NodeType.FULL.value:
        setupFull()
    else:
        setupClient()


def installBinary(name, version, url, prompt, cmd):
    colorprint(prompt)
    os.chdir(os.path.expanduser(HOME))
    gitClone = subprocess.Popen(
        ["git clone {} {}".format(url, name)], stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, text=True, shell=True)
    if "Repository not found" in gitClone.communicate()[1]:
        colorprint(url + """ repo provided by user does not exist, try another URL
        """)
        quit()
    os.chdir(os.path.expanduser(HOME+"/"+name))
    subprocess.run(["git stash"], stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL, shell=True)
    subprocess.run(["git pull"], stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL, shell=True)
    gitCheckout = subprocess.Popen(["git checkout {v}".format(
        v=version)], stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, text=True, shell=True)
    if "did not match any file(s) known to git" in gitCheckout.communicate()[1]:
        colorprint(version + """ branch provided by user does not exist, try another branch)
        """)
        quit()

    runCMD("make " + cmd, "Successfully installed "+name, "Failed to install "+name)


def checkGo(version):
    check = subprocess.Popen(["go env GOVERSION"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, shell=True, env=my_env)
    output = check.communicate()
    if output[1]:
        return False
    if not (version in output[0]):
        return False
    check = subprocess.Popen(["go env GOPATH"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, shell=True, env=my_env)
    return check.communicate()[0].strip() == GOPATH

def installGo(version):
    if checkGo(version):
        return
    runCMD("wget https://git.io/go-installer.sh", "", "Failed to get Go installer")
    runCMD("bash go-installer.sh remove", "", "Failed to uninstall Go")
    runCMD("bash go-installer.sh --version {}".format(config.get('goVersion', '1.21.1')), "", "Failed to install Go "+version)

def initSetup():
    joltifyName = config.get('joltifyName', 'joltify')
    if os_name == "Linux":
        colorprint("Please wait while the following processes run:")
        colorprint("(1/4) Updating Packages...")
        subprocess.run([sudoprefix + "apt-get update"],
                       stdout=subprocess.DEVNULL, shell=True)
        colorprint("(2/4) Installing make and GCC...")
        subprocess.run([sudoprefix + "apt install git build-essential ufw curl jq snapd supervisor -y"], shell=True)
        subprocess.run(["ps axo pid,command|grep supervisxord|grep -v grep || {sudo}systemctl start supervisor.service".format(sudo=sudoprefix)],
                       stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
        subprocess.run(["{}supervisord -c /etc/supervisor/supervisord.conf ".format(sudoprefix)], shell=True,
                       stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                
        colorprint("(3/4) Installing Go...")
        installGo(config.get('goVersion', '1.22.3'))

        installBinary(joltifyName, config.get('version'), repo, "(4/4) Installing Joltify {v} Binary...".format(v=config.get('version')), "install")
    else:
        colorprint("The installer does not support other OSs except 'Linux'.")
        quit()

    if nodeType == NodeType.FULL.value:
        waitForContinue("Press any key to continue...")
        mainMenu()


def mainMenu():
    subprocess.run(["clear"], shell=True)
    colorprint("""Please choose an option you want to run:
1) Run a Joltify Full Node
2) Check Joltify Node sync status
3) Become a Joltify Validator (require a synced full node)
4) Run a Joltify Bridge Service (not available)
5) Quit
""")
    choice = input(bcolors.OKGREEN + "Choose an option: " + bcolors.ENDC)
    if choice == "1":
        initNodeName()
    elif choice == "2":
        checkSyncProgress()
        mainMenu()
    elif choice == "3":
        subprocess.run(["clear"], shell=True)
        if isSynced():
            jolt_home = config.get('installHome')
            backupKeys(jolt_home+"/keyring-file")
            runCMD("rm -rf " + jolt_home + "/keyring-file", "", "Failed to remove "+ jolt_home + "/keyring-file")
            setupKeys()
            joinValidator()
        else:
            waitForContinue("Your node is not synced yet.\nPlease wait for the node to be synced.\nYou can check the sync status by selecting option '2'.\nPress any key to continue...")
            mainMenu()
        mainMenu()
    elif choice == "4":
        subprocess.run(["clear"], shell=True)
        waitForContinue("Joltify Bridge Service is currently not available.\nPress any key to continue...")
        # bridgeService()
        mainMenu()
    elif choice in ["5", "q", "Q"]:
        subprocess.run(["clear"], shell=True)
        quit()
    else:
        mainMenu()


def swapSet(size):
    if not os.path.exists('/swapfile'):
        return False
    stat = os.stat('/swapfile')
    file_gib = stat.st_size/(1024.**3)
    return round(file_gib) >= size


def setupSwap(size, swithOn):
    if os_name != "Linux":
        subprocess.run(["clear"], shell=True)
        print(bcolors.WARNING + "The installer does not support other OSs except 'Linux'." + bcolors.ENDC)
        return

    if not swithOn:
        return

    mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
    mem_gib = mem_bytes/(1024.**3)
    colorprint("RAM Detected: "+str(round(mem_gib))+"GB")
    swapNeeded = size - round(mem_gib)
    if swapNeeded <= 0:
        colorprint(
            "You have enough RAM to meet the {}GB minimum requirement, moving on to system setup...".format(size))
        time.sleep(3)
        subprocess.run(["clear"], shell=True)
        return

    if swapSet(size):
        colorprint(
            "{}GB swap file set, moving on to system setup...".format(size))
        time.sleep(3)
        subprocess.run(["clear"], shell=True)
        return
    
    print(bcolors.OKGREEN + "Setting up " + str(swapNeeded) + "GB swap file..." + bcolors.ENDC)
    subprocess.run([sudoprefix + "swapoff -a"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    subprocess.run([sudoprefix + "fallocate -l " + str(swapNeeded)+"G /swapfile"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    subprocess.run([sudoprefix + "chmod 600 /swapfile"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    subprocess.run([sudoprefix + "mkswap /swapfile"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    subprocess.run([sudoprefix + "swapon /swapfile"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    subprocess.run([sudoprefix + "cp /etc/fstab /etc/fstab.bak"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    subprocess.run(["echo '/swapfile none swap sw 0 0' | {sudo}tee -a /etc/fstab".format(sudo=sudoprefix)], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=True)
    print(bcolors.OKGREEN + str(swapNeeded) + "GB swap file set" + bcolors.ENDC)
    time.sleep(3)
    subprocess.run(["clear"], shell=True)


def initEnvironment():
    swapOn = strtobool(config.get('swapOn', 'true'))
    setupSwap(32, swapOn)
    initSetup()


def waitForContinue(prompt):
    input(bcolors.OKGREEN + prompt + bcolors.ENDC)


def inputYesNo(prompt):
    while True:
        subprocess.run(["clear"], shell=True)
        ans = input(bcolors.OKGREEN + prompt + "\nPlease enter yes or no (y/yes or n/no):" + bcolors.ENDC)
        if ans.lower() in ['y', 'yes']:
            return True
        elif ans.lower() in ['n', 'no']:
            return False


def hasPortConfig(cfg = {}):
    for key in defaultPorts.keys():
        if cfg.get(key, None):
            return True
    return False


def getServiceAddr(addr):
    addrs = addr.split("//")
    addr = addrs[len(addrs) - 1]
    if addr.startswith("0.0.0.0"):
        addr = addr.replace("0.0.0.0", "127.0.0.1")
    return addr


def loadConfig():
    subprocess.run(["clear"], shell=True)
    global config
    global cfgName
    global networkType
    global customPortSet
    network = None
    cfgName = None
    customPortSet = False
    for file in os.scandir("."):
        if not (file.is_file() and file.name.startswith('.env.')):
            continue
        network = file.name[len('.env.'):]
        if network in [NetworkType.MAINNET.value, NetworkType.TESTNET.value]:
            if inputYesNo("Do you want to load config from {}?".format(file.name)):
                cfgName = file.name
                networkType = network
                break
    if cfgName:
        cfg = dotenv_values(cfgName)
        customPortSet = hasPortConfig(cfg)
        config = {
            **cfg,
        }
        return
    colorprint("""
Please ensure that one of the following files exists in the current folder:
    Mainnet: .env.{}
    Testnet: .env.{}
""".format(NetworkType.MAINNET.value, NetworkType.TESTNET.value))
    reloadOrQuit = input(bcolors.OKGREEN + """
 Press any key to reload or Input (q)uit to quit: """ + bcolors.ENDC)
    if reloadOrQuit.lower() == "q":
        subprocess.run(["clear"], shell=True)
        quit()
    loadConfig()

def start():
    def restart():
        global HOME
        global USER
        global GOPATH
        global machine
        global os_name
        global nodeType
        global currentDir
        global sudoprefix
        global my_env
        currentDir = os.getcwd()
        os_name = platform.system()
        if os_name == "Linux":
            username = pwd.getpwuid(os.getuid()).pw_name
            sudoprefix = "sudo "
            if username == "root":
                sudoprefix = ""
        machine = platform.machine()
        HOME = subprocess.run(
            ["echo $HOME"], capture_output=True, shell=True, text=True).stdout.strip()
        USER = subprocess.run(
            ["whoami"], capture_output=True, shell=True, text=True).stdout.strip()
        GOPATH = HOME+"/go"
        my_env = os.environ.copy()
        my_env["PATH"] = "{gp}/bin:{h}/.go/bin:{p}".format(gp=GOPATH, h=HOME, p=my_env["PATH"])

        print(bcolors.OKGREEN + """
                         _       _ _   _  __          ____ _           _       
                        | | ___ | | |_(_)/ _|_   _   / ___| |__   __ _(_)_ __  
                     _  | |/ _ \| | __| | |_| | | | | |   | '_ \ / _` | | '_ \ 
                    | |_| | (_) | | |_| |  _| |_| | | |___| | | | (_| | | | | |
                     \___/ \___/|_|\__|_|_|  \__, |  \____|_| |_|\__,_|_|_| |_|
                                             |___/                             

    Welcome to the Joltify installer!

    For more information, please visit docs.joltify.io
    Ensure no joltify services are running in the background
    If running over an old joltify installation, back up
    any important joltify data before proceeding.
        """ + bcolors.ENDC)
        waitForContinue("Press any key to continue...")
        loadConfig()
        nodeType = config.get('nodeType')

        while True:
            subprocess.run(["clear"], shell=True)
            colorprint("1) Start Installing\n2) Go to Main Menu\n3) Quit\n")
            choice = input(bcolors.OKGREEN + "Enter choice: " + bcolors.ENDC)
            if choice == "1":
                subprocess.run(["clear"], shell=True)
                break
            elif choice == "2":
                subprocess.run(["clear"], shell=True)
                mainMenu()
            elif choice == "3":
                subprocess.run(["clear"], shell=True)
                quit()
            else:
                restart()

        if nodeType == NodeType.FULL.value:
            initEnvironment()
        elif nodeType == NodeType.CLIENT.value:
            initSetup()
            initNodeName()
        else:
            waitForContinue("Invalid nodeType[{}] in {}, please recorrect it.".format(nodeType))
            restart()
    
    try:
        subprocess.run(["clear"], shell=True)
        restart()
    except KeyboardInterrupt:
        subprocess.run(["clear"], shell=True)
        print(bcolors.WARNING + "Keyboard Interrupt!!" + bcolors.ENDC)
        quit()

start()
